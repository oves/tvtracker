package tvtracker

import (
	"strings"

	"github.com/nlopes/slack"
)

// MONITOR is const for monitoring section in the json configuration
const MONITOR = "monitor"

// TOKEN is clack bot token
const TOKEN = "token"

// CHANNEL is slack channel
const CHANNEL = "channel"

// Notifier is a basic interface for the update notifications
type Notifier interface {
	Monitor(string) bool
	Notify(string, string)
}

// SlackNotifier notifies via Slack
type SlackNotifier struct {
	monitor map[string]int
	bot     *slack.Client
	channel string
}

// Notify sends string message
func (sn SlackNotifier) Notify(title string, body string) {
	if sn.bot != nil {
		params := slack.PostMessageParameters{}
		attachment := slack.Attachment{
			Text: body,
		}
		params.Attachments = []slack.Attachment{attachment}
		sn.bot.PostMessage(sn.channel, title, params)
	}
}

// Monitor checks if it is objecct of interes
func (sn SlackNotifier) Monitor(check string) bool {
	_, ok := sn.monitor[strings.ToLower(check)]
	return ok
}

// NewSlackNotifier creates new SlackNotifier object
func NewSlackNotifier(cfg map[string]interface{}) Notifier {
	// prepare map for objects of interest
	look := make(map[string]int)
	keys, ok := cfg[MONITOR]
	if ok {
		for _, v := range keys.([]interface{}) {
			look[strings.ToLower(v.(string))] = 1
		}
	}

	// init slack bot
	var api *slack.Client
	var channel string
	token, ok := cfg[TOKEN]
	if ok {
		api = slack.New(token.(string))
	}
	ch, ok := cfg[CHANNEL]
	if ok {
		channel = ch.(string)
	}

	return Notifier(SlackNotifier{look, api, channel})
}

// CreateNotificator type to cretae a notifier
type CreateNotificator func(map[string]interface{}) Notifier

// Notifiers is a map of listeners
var Notifiers = map[string]CreateNotificator{
	"slack": NewSlackNotifier,
}
