package tvtracker

import (
	"regexp"
	"strings"

	"github.com/SlyMarbo/rss"
	"github.com/boltdb/bolt"
)

// Logger is the interface to Debug, Info, Error and Warn loging methods
type Logger interface {
	Debug(args ...interface{})
	Info(args ...interface{})
	Error(args ...interface{})
	Warn(args ...interface{})
}

// Options is a configuration of tracker
type Options struct {
	Rss string `json:"rss" yaml:"rss"`
	Db  struct {
		Data  string `json:"data" yaml:"data"`
		Index string `json:"index" yaml:"index"`
	}
	Ignore []string
	Notify map[string]interface{} `json:"notify" yaml:"notify"`
}

// Tracker is base type
type Tracker struct {
	op   Options
	log  Logger
	voc  map[string]int // vocabulary
	news map[string][]string
}

// New creates an Tracker object
func New(opt Options, log Logger) *Tracker {
	voc := make(map[string]int)

	for _, val := range opt.Ignore {
		voc[val] = 1
	}

	return &Tracker{opt, log, voc, nil}
}

func (track *Tracker) filter(keys []string) []string {
	var nkeys []string

	//
	for _, key := range keys {
		_, ifVoc := track.voc[key]
		if len(key) > 2 && ifVoc == false {
			nkeys = append(nkeys, key)
		}
	}
	return nkeys
}

func (track *Tracker) parse(title string) (Episode, bool) {
	regs := []string{"S([0-9]+)E([0-9]+)"} // "([0-9]{4}) ([0-9]{2}) ([0-9]{2})"
	var item Episode
	// Find title & info
	success := false
	for _, reg := range regs {
		exp, _ := regexp.Compile(reg)
		mark := exp.FindStringIndex(title)
		if mark != nil {
			success = true
			//
			item.TV = title[:mark[0]-1]
			item.Keys = track.filter(strings.Split(strings.ToLower(item.TV), " "))
			// Season
			exp, _ := regexp.Compile("S([0-9]+)")
			s := exp.FindString(title[mark[0]:mark[1]])
			item.Season = s[1:]
			// Episode
			exp, _ = regexp.Compile("E([0-9]+)")
			e := exp.FindString(title[mark[0]:mark[1]])
			item.Episode = e[1:]
			break
		}
	}

	return item, success
}

// Start reading he RSS
func (track *Tracker) Read() {

	track.log.Info("Reading: ", track)
	// Reaad feed
	channel, err := rss.Fetch(track.op.Rss)
	if err != nil {
		track.log.Error("rss.Fetch: ", err)
		return
	}

	// Open db
	db, err := bolt.Open(track.op.Db.Data, 0600, nil)
	if err != nil {
		track.log.Error(err)
		return
	}
	defer db.Close()

	// Parse & store
	track.log.Info(channel.Title, ": ", len(channel.Items))
	// log new items
	track.news = make(map[string][]string)

	for _, item := range channel.Items {
		ep, fail := track.parse(item.Title)
		if fail == false {
			track.log.Warn("NOT parsed: ", item.Title)
			continue
		}
		ep.Torrent = Torrent{item.Title, item.Enclosures[0].URL, uint64(item.Enclosures[0].Length), item.Date}
		track.log.Debug(ep)
		num := track.store(db, ep)
		if num {
			track.log.Info("Add: ", ep.TV, " | ", string(ep.epID()))
			track.news[string(ep.tvID())] = append(track.news[string(ep.tvID())], ep.Torrent.String())
		}
	}
}

// Notify starts notification about new items
func (track *Tracker) Notify() {
	for k, v := range track.op.Notify {
		createNoty, ok := Notifiers[k]
		// No correspondinf notifier
		if ok == false {
			track.log.Error("Wrong notifier: ", k)
			continue
		}

		noty := createNoty(v.(map[string]interface{}))
		for param, msg := range track.news {
			if noty.Monitor(param) {
				noty.Notify(param, strings.Join(msg, "\n"))
			}
		}
	}
}

func (track *Tracker) updateNestedBucket(db *bolt.DB, bID1 []byte, bID2 []byte, key []byte, value []byte) (bool, error) {
	count := false
	err := db.Update(func(tx *bolt.Tx) error {
		var err error
		var bucket1, bucket2 *bolt.Bucket
		bucket1, err = tx.CreateBucketIfNotExists(bID1)
		if err == nil {
			bucket2, err = bucket1.CreateBucketIfNotExists(bID2)
			if err == nil {
				if bucket2.Get(key) == nil {
					bucket2.Put(key, value)
					count = true
				}
			}
		}
		return err
	})
	return count, err
}

func (track *Tracker) createIndex(keys []string, show []byte) {
	db, err := bolt.Open(track.op.Db.Index, 0600, nil)

	if err != nil {
		track.log.Error(err)
		return
	}
	defer db.Close()

	// start update
	for _, key := range keys {
		_, err = track.updateNestedBucket(db, []byte(key[:1]), []byte(key), show, []byte{0x1})
	}
}

func (track *Tracker) store(db *bolt.DB, ep Episode) bool {
	// preapre data
	show := ep.tvID()
	episode := ep.epID()
	enc, err := ep.Torrent.Bytes()
	size := ep.Torrent.size()
	count := false

	// encoding error
	if err != nil {
		track.log.Error(err)
		return false
	}

	// Update db
	count, err = track.updateNestedBucket(db, show, episode, size, enc)

	if err != nil {
		track.log.Error(err)
		return false
	}

	// Create index
	track.createIndex(ep.Keys, show)

	return count
}
