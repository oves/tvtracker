package tvtracker

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"strings"
	"time"
)

// Torrent is torrent file information
type Torrent struct {
	Name string
	File string
	Size uint64
	Date time.Time
}

// Episode information
type Episode struct {
	TV      string
	Season  string
	Episode string
	Keys    []string
	Torrent Torrent
}

func (ep *Episode) tvID() []byte {
	return []byte(strings.ToLower(ep.TV))
}

func (ep *Episode) epID() []byte {
	return []byte(fmt.Sprintf("S%sE%s", ep.Season, ep.Episode))
}

func (tor *Torrent) size() []byte {
	return []byte(fmt.Sprintf("%v", tor.Size))
}

// Bytes returns []byte representation
func (tor *Torrent) Bytes() ([]byte, error) {
	// create map
	sm := make(map[string]interface{})
	sm["Name"] = tor.Name
	sm["File"] = tor.File
	sm["Size"] = tor.Size
	sm["Date"] = tor.Date.String()

	// convert map to []byte
	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	err := enc.Encode(&sm)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// String returns string representation
func (tor *Torrent) String() string {
	size := float32(tor.Size) / 1024 / 1024
	return fmt.Sprintf("%s\n %.1fMb %s", tor.File, size, tor.Date.Format("2006-01-02 15:04:05"))
}
