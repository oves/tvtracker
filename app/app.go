package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path"

	"bitbucket.org/oves/tvtracker"

	"github.com/ghodss/yaml"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"gopkg.in/urfave/cli.v1"
)

var (
	errUnsupoortedFileType = errors.New("Unsupported file type. JSON or YAML is required")
	strJSON                = ".json"
	strYAML                = ".yaml"
)

type configLog struct {
	Level string `json:"level" yaml:"level"`
	Path  string `json:"file" yaml:"file"`
}

type config struct {
	Main tvtracker.Options `json:"main" yaml:"main"`
	Log  configLog         `json:"log" yaml:"log"`
}

func readConfig(cfgPath string) (config, error) {
	var cfg config
	file, err := ioutil.ReadFile(cfgPath)
	// if error
	if err != nil {
		return cfg, err
	}

	if path.Ext(cfgPath) == strJSON {
		json.Unmarshal(file, &cfg)
	} else if path.Ext(cfgPath) == strYAML {
		yaml.Unmarshal(file, &cfg)
	} else {
		return cfg, errUnsupoortedFileType
	}

	return cfg, err
}

// Logger
var ogr = logrus.New()

//
func init() {
	//
	formatter := new(logrus.TextFormatter)
	formatter.TimestampFormat = "15:04:05.000"
	formatter.FullTimestamp = true
	//
	ogr.Formatter = formatter
	ogr.Level = logrus.ErrorLevel
}

//
func action(c *cli.Context) error {
	var cfg config
	var err error
	// Read config parameters
	if path := c.String("config"); path != "" {
		cfg, err = readConfig(path)
		if err != nil {
			return err
		}
		// Log
		ogr.Level, _ = logrus.ParseLevel(cfg.Log.Level)
		if cfg.Log.Path != "" {
			ogr.Hooks.Add(lfshook.NewHook(lfshook.PathMap{
				logrus.ErrorLevel: cfg.Log.Path,
			}))
		}
	}
	// Read command line parameters
	if debug := c.String("debug"); debug != "" {
		ogr.Level, _ = logrus.ParseLevel(debug)
	}

	//
	tracker := tvtracker.New(cfg.Main, ogr)
	tracker.Read()
	tracker.Notify()

	return nil
}

//
func newApp() *cli.App {
	app := cli.NewApp()
	app.Name = "TV tracker"
	app.Usage = "read, parse and index"
	app.Version = "0.0.3"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Usage: "load configuration from `FILE`",
			Value: "config.json",
		},
		cli.StringFlag{
			Name:  "debug, d",
			Usage: "debug `LEVEL`",
		},
	}

	app.Action = action

	return app
}

func main() {
	app := newApp()
	app.Run(os.Args)
}
